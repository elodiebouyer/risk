#include "Utils.hpp"
#include "Joueur.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>

using namespace std;

string intToString(int entier)
{
  ostringstream oss;
  oss << entier;
  return oss.str();
}

string colorToString(Color c)
{
  switch(c)
  {
    case Blue: return "blue";
    case Pink: return "pink";
    case Red: return "red";
    case Brown: return "brown";
    case Orange: return "orange";
    case Yellow: return "yellow";
    case Green: return "green"; 
  }
}


void printColors()
{
  cout << "\t0-Blue" << endl;
  cout << "\t1-Red" << endl;
  cout << "\t2-Green" << endl;
  cout << "\t3-Pink" << endl;
  cout << "\t4-Brown" << endl;
  cout << "\t5-Orange" << endl;
  cout << "\t6-Yellow" << endl;
  
}

int howManyPlayer()
{
  #ifdef SCENARIO
  return 2;
  #else

    int nbJoueur = 0;
    bool ok = true;
    cout << "Combien de joueur ?" << endl;
    while ( !(cin >> nbJoueur) )
      {
        cout << "Saisie incorrecte, recommencez : "; 
        cin.clear(); 
        cin.ignore( numeric_limits<streamsize>::max(), '\n' );
      }
    return nbJoueur;

  #endif
}

void printMap(Territory_c *ters, int nbTerritories, vector<Link_territories> links, Player * players, int nbPlayers, int etape)
{
  #ifndef SCENARIO
  string name = "etape" + intToString(etape) +".dot";
  ofstream outfile(name.c_str());
  string buffer = "graph G{ \n"; // Première ligne à écrire.

  for(int i = 0 ; i < nbTerritories ; i++)
  {
    buffer += intToString(ters[i].id);
    buffer += "[ color=";
    if( ters[i].player < 1) buffer += "white";
    else buffer += colorToString(getColorPlayer(players, nbPlayers, ters[i].player));
    buffer += ", shape=record, label=\"{";
    buffer += intToString(ters[i].id) + "|";
    buffer += intToString(ters[i].units);
    buffer += "} \" ]; \n";
  }

  for(int i = 0 ; i < links.size() ; i++)
  {
    buffer += intToString(links[i].idA);
    buffer += " -- ";
    buffer += intToString(links[i].idB);
    buffer += "\n";
  }

  buffer += "}";

  outfile.write(buffer.c_str(), buffer.length());
  outfile.close();

  string pdf = "etape" + intToString(etape) + ".pdf";
  string dot = " etape" + intToString(etape) + ".dot";
  string commande = "dot -Tpdf -o " + pdf + dot;
  system(commande.c_str());
  commande = "xdot"+dot+ " &";
  system(commande.c_str());
  #endif
}

vector<Territory_c> loadMap(vector<Link_territories>* links)
{
  #ifdef SCENARIO
  string map = "map_senario.dot";
  #else
  string map = "map.dot";
  #endif

  vector<Territory_c> territoires;
  int linkA, linkB; // point A relié à B, B relié a A aussi.

  std::ifstream infile(map.c_str());
  string line;
  std::getline(infile, line); // première ligne, on en a pas besoin
	

  while (std::getline(infile, line))
  {
    if(line.find("[")!=std::string::npos)
	  {
      territoires.push_back(createTerritory(atoi(line.c_str())));
	  }

      else if(line.find("--")!=std::string::npos)
	{
	  std::istringstream iss(line);
	  iss >> linkA;
	  iss.ignore(10, ' ');
	  iss.ignore(10, '-');
	  iss.ignore(10, ' ');
	  iss >> linkB;
	  Link_territories * l = new Link_territories();
	  l->idA = linkA;
	  l->idB = linkB;
	  links->push_back(*l);
	}	    
    }

  return territoires;
}

void afficherTerritoires(int nb, Territory_c * territories)
{
  for(int i = 0 ; i < nb ; i++)
    printTerritory(territories[i]);
}


void afficherLink(int nb, Link_territories* links)
{
  //affichage des liens initiaux
  for(int i = 0; i < nb; i++)
    cout << links[i].idA << " <--> " << links[i].idB << endl;
}

void afficherPlayers(int nb, Player* p)
{
  cout << "Liste des joueurs inscrits : " << endl;
  for(int i = 0; i < nb; i++)
    {
      printPlayerName(p[i]);
      printPlayerTerritories(p[i]);
    }
}

void buildMouvement(MPI_Datatype * m)
{
  int block_lengths[5]={1,1,1,1,1};
  MPI_Aint deplacement[5];
     
  MPI_Datatype typelists[5] = {MPI_INT,MPI_INT,MPI_INT,MPI_INT,MPI_INT};
  MPI_Aint start_address;
  MPI_Aint address;
 
  //Calculate Displacements
  deplacement[0] = offsetof(Mouvement, player);
  deplacement[1] = offsetof(Mouvement, depart);
  deplacement[2] = offsetof(Mouvement, arrivee);
  deplacement[3] = offsetof(Mouvement, nbUnites);
  deplacement[4] = offsetof(Mouvement, type);

  //Create and Commit new mpi type
  MPI_Type_create_struct(5, block_lengths,deplacement,typelists,m);
    
}

void afficherOrdre(Mouvement* mvs, int nb, int pid)
{
  for(int i = 0 ; i < nb ; i++)
    cout << "(" << pid << ") "<<"Joueur " << mvs[i].player << " : déplace de " << mvs[i].depart << " " << mvs[i].nbUnites<< " unités, vers " << mvs[i].arrivee << endl;
 }

void infoPlayer(Territory_c * territories, int nbTerritories, int id, int & nbUnites)
{
  for(int i = 0 ; i < nbTerritories ; i++)
  {
    if( territories[i].player == id)
    {
      nbUnites += territories[i].units;
    }
  }
}


#ifndef SCENARIO
  void mouvementTroupes(Territory_c * terri, int nbTerri, Link_territories * links,int nbLinks,Player p, Mouvement & m)
  {
    int depart = 0, arrivee = 0, nbUnites = 0;

    cout << endl << p.name << " où voulez-vous déplacer vos troupes ? " << endl;


    cout << "Territoire de départ = ";
    while( !(cin >> depart)             // On vérifie que la saisie est correcte.
           || !aTerritoire(p,depart))   // On vérifie qu'il dispose de ce territoire.
    {
      cout << "Mauvaise saisie (disposez-vous de ce territoire ?), recommencez : "; 
      cin.clear();
      cin.ignore( numeric_limits<streamsize>::max(), '\n' );
    }

    cout << endl << "Territoire d'arrivée = ";
    while( !(cin >> arrivee)                            // On vérifie que la saisie est correcte.
           || !passage(links,nbLinks,depart,arrivee)) { // On vérifie qu'il existe un lien entre les deux territoires.
      cout << "Mauvaise saisie (existe-t-il un lien entre ces deux territoires ?), recommencez : ";
      cin.clear();
      cin.ignore( numeric_limits<streamsize>::max(), '\n' );
    }
    
    cout << endl << "Nombre d'unités à déplacer = ";
    while( !(cin >> nbUnites)                         // On vérifie que la saisie est correcte.  
           || !uniteSuffisante(terri, nbTerri, depart, nbUnites) ) {  // On vérifie qu'il dispose d'assez d'unité.
      cout << "Mauvaise saisie (pouvez-vous déplacer autant de troupe ?), recommencez : ";
      cin.clear();
      cin.ignore( numeric_limits<streamsize>::max(), '\n' );
    }
    
    m.player = p.id;
    m.depart = depart;
    m.arrivee = arrivee;
    m.nbUnites = nbUnites;
  }
#else
  void mouvementTroupesScenario(Territory_c * terri, int nbTerri, Link_territories * links,int nbLinks,Player p, Mouvement & m)
  {
    int depart = 0, arrivee = 0, nbUnites = 0;
    static ifstream fichier("scenario.txt", ios::in);
    if(fichier.eof()){
      cout << "EOF" << endl;
      return;
    }
    fichier >> depart;
    cout << depart; 
    fichier >> arrivee;
    cout << arrivee; 
    fichier >> nbUnites;

    //cout << p.id << " part de " << depart << " à " << arrivee << " avec " << nbUnites << " unités " << endl;
    
    m.player = p.id;
    m.depart = depart;
    m.arrivee = arrivee;
    m.nbUnites = nbUnites;
  }
#endif

bool isIn(int * voisins, int nb, int id)
{
  if( nb == 0 ) return false;
  for(int i = 0 ; i < nb ; i++)
  {
    if( voisins[i] == id ) return true;
  }
  return false;
}

void afficherGrappe(int * voisin, int nbVoisin)
{
  for(int i = 0 ; i < nbVoisin ; i++)
  {
    cout << voisin[i] << " ";
  }
}

void voisins(int player, int id, 
             Link_territories *links, int nbLinks, 
             Territory_c *territories, int nbTerritoriesTotal,
             int * voisin, int & taille, int & nbEnnemi)
{
  bool stop = true;
  int proprietaire = -1;

  if(taille < 0 ) return;

  for(int i = 0 ; i < nbLinks ; i++)
  {
    if( links[i].idA == id )
    {
      if( estAuJoueur(player, links[i].idB, territories, nbTerritoriesTotal, proprietaire) ) 
      {
        if( !isIn(voisin, taille, links[i].idB ) )
        {
          taille++;
          voisin = (int*) realloc(voisin, sizeof(int)*taille);
          voisin[taille-1] = links[i].idB;
          stop = false;
        }
      }
      else if ( proprietaire != -1 )
      {
          nbEnnemi++;
      }
    }
    else if( links[i].idB == id )
    {
      if( estAuJoueur(player, links[i].idA, territories, nbTerritoriesTotal, proprietaire) )
      {
        if( !isIn(voisin, taille, links[i].idA ) )
        {
          taille++;
          voisin = (int*) realloc(voisin, sizeof(int)*taille);
          voisin[taille-1] = links[i].idA;
          stop = false;
        }
      }
      else if ( proprietaire != -1 )
      {
          nbEnnemi++;
      }
    }
  }

  if( !stop )
  {
    for(int i = 0 ; i < taille ; i++)
    {
      voisins(player, voisin[i], links, nbLinks, territories, nbTerritoriesTotal, voisin, taille, nbEnnemi);
    }
  }
}

void connexe(int player, Territory_c *territories, int nbTerritoriesTotal, 
                         Territory_c *territories_recv, int nbTerritories, 
                         Link_territories *links, int nbLinks, Player & p)
{
  int nbVoisin = 0, nbEnnemi;
  int * voisin = NULL;
  int pid, nbProcs;

  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

  // Chaque processus parcourt les territoires qu'il possède.
  for(int i = 0 ; i < nbTerritories ; i++)
  {
    // On regarde si le territoire appartient au joueur.
    if( player == territories_recv[i].player )
    { 
      voisins(player, territories_recv[i].id, links, nbLinks, territories, nbTerritoriesTotal, voisin, nbVoisin, nbEnnemi);
      cout << "Apres la fonction voisins, nbVoisin=" << nbVoisin << endl;
      for(int i = 0 ; i < nbVoisin ; i++) cout << voisin[i] << " ";
        cout << endl;
      if( nbVoisin > 0 )
      {
        nbVoisin++;
        voisin = (int*) realloc(voisin, sizeof(int)*(nbVoisin));
        voisin[nbVoisin-1] = -1;
      }
    }
  }

  if( pid != 0) 
  {
    MPI_Send(&nbVoisin, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
    //cout << "pid "<< pid << " a envoyé nb voisin = " << nbVoisin << endl;
    if( nbVoisin > 0 ) MPI_Send(voisin, nbVoisin, MPI_INT, 0, 1, MPI_COMM_WORLD);

  }
  else
  {
    // Le processus 0 s'occupe de ses données avant de recevoir celles des autres.
    if( nbVoisin > 0 ) addConnexes(p, voisin, nbVoisin);

    for(int j = 1 ; j < nbProcs ; j++)
    {

      // Il reçoie ensuite les info sur les voisins connexe.
      MPI_Recv(&nbVoisin, 1, MPI_INT, j, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      //cout << "0 à recu nb voisin = " << nbVoisin << endl;

      if( nbVoisin > 0 ) 
      {
        voisin = new int[nbVoisin];
        MPI_Recv(voisin, nbVoisin, MPI_INT, j, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

       // for(int i = 0 ; i < nbVoisin ; i++) cout << voisin[i] << " ";
        //cout << endl;

        addConnexes(p, voisin, nbVoisin);
        delete [] voisin;
        voisin = NULL;
      }
    }
  }
  if( voisin != NULL) delete [] voisin;
}