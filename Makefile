CXX = mpic++
CFLAGS = -O2
LDFLAGS = 

SRC = $(wildcard *.cpp)
OBJS = $(SRC:.cpp=.o)
AOUT = risk

all : $(AOUT) 

SCENARIO : 
	$(CXX) -DSCENARIO GameMaster.cpp Joueur.cpp Territoire.cpp Utils.cpp -o $(AOUT)
$(AOUT) : $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^
%.o : %.cpp
	@$(CXX) $(CFLAGS) -o $@ -c $<


clean :
	@rm -f *.o *.*~ etape*
cleaner : clean
	@rm $(AOUT)
.PHONY: cleaner