#include <iostream>
#include <mpi.h>
#include <string>
#include <stdlib.h> 
#include <time.h>
#include <limits>

#include "Utils.hpp"

using namespace std;

int main(int argc, char **argv)
{
  int pid, nbProcs, finJeu = 0, etape = 1;
  int nbPlayers = 0, nbTerritories = 0, nbTerritoriesTotal=0, nbLinks = 0, nbLinksTotal=0, nbMvs = 0;
  vector<Territory_c> territoires, copy_terri;
  vector<Link_territories> links_cpp;
  Territory_c * territories;
  Player * players;
  Link_territories * links;
  Mouvement * mvs;
  vector<Mouvement> mouv;



  /*******************************************
   *** Début de la programmation parallèle.***
   *******************************************/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size(MPI_COMM_WORLD, &nbProcs);

  // Pour les différents modes
  #ifdef SCENARIO
    cout << "Mode Sénario." << endl;
  #endif



  /*******************************************
   ******* Création des nouveaux types. ******
   *******************************************/
  MPI_Datatype MPI_TERRITORY;
  buildTerritoryType(&MPI_TERRITORY);
  MPI_Type_commit(&MPI_TERRITORY);

  MPI_Datatype MPI_LINK;
  buildLinkType(&MPI_LINK);
  MPI_Type_commit(&MPI_LINK);

  MPI_Datatype MPI_MOUVEMENT;
  buildMouvement(&MPI_MOUVEMENT);
  MPI_Type_commit(&MPI_MOUVEMENT);


  if( pid == 0 )
  { 

    /*******************************************
     ******** Initialisation des joueurs. ******
     *******************************************/
    nbPlayers = howManyPlayer();
    players = new Player [nbPlayers];
    askPlayers(nbPlayers, players);


    /*******************************************
      ****** Initialisation des territoires. ****
      *******************************************/
    territoires = loadMap(&links_cpp);
    territories = &territoires.front();
    nbTerritories = territoires.size();
    nbTerritoriesTotal = nbTerritories;
    nbTerritories = 0;
    links = &links_cpp.front(); 
    nbLinks = links_cpp.size();
    nbLinksTotal = nbLinks;

    if( nbTerritoriesTotal < (5*(nbPlayers+2)))
    {
      cout << "La carte n'est pas assez grande pour " << nbPlayers << " joueurs ." <<  endl;
      MPI_Finalize();
      return 0;
    }

    /******************************************************
     *** Attribution des premiers territoires aux joueurs.*
     ******************************************************/
    copy_terri = territoires;
    for( int i = 0 ; i < nbPlayers ; i++)
	  {
	    srand (time(NULL));
	    for(int j = 0; j < FIRST_TERRITORIES ; j++)
	    {
        #ifdef SCENARIO
          // En mode sénario on attribue les n premiers territoires (id 1 à n).
          static int index = 0;
          index++;
          cout << copy_terri[index].id << endl;
          ajouterTerritoire(players, nbPlayers, players[i].id, index);
          territories[index].player = players[i].id;
          territories[index].units = FIRST_UNITS;
        #else
	       int index = rand() % copy_terri.size();
        ajouterTerritoire(players, nbPlayers, players[i].id, copy_terri[index].id);
        territories[copy_terri[index].id].player = players[i].id;
        territories[copy_terri[index].id].units = FIRST_UNITS;
        copy_terri.erase(copy_terri.begin()+index);
        #endif
	      
	    }
	  }
  }


  /*********************************************************
   ***** Distribution de territoire à tous les processus. **
   *********************************************************/
  MPI_Bcast(&nbTerritoriesTotal, 1, MPI_INT, 0, MPI_COMM_WORLD);
  int countSend = (int) nbTerritoriesTotal / nbProcs;
  Territory_c * territories_recv = new Territory_c [countSend];
  
  if( pid != 0 )
    nbTerritories = countSend;

  MPI_Scatter(territories, countSend, MPI_TERRITORY, 
	            territories_recv, countSend, MPI_TERRITORY, 0, MPI_COMM_WORLD); 
  
  if(pid == 0)
    {
      int nbTerrRestant = nbTerritoriesTotal - (countSend*(nbProcs-1))-countSend;

      if( nbTerrRestant > 0 )
      {
        Territory_c * terAux = (Territory_c*) realloc(territories_recv,sizeof(Territory_c)*(countSend+nbTerrRestant));
        territories_recv = terAux;
        for(int i = 0 ; i < countSend ; i++ ) territories_recv[i] = territories[i]; 
        for(int i = nbTerritoriesTotal - nbTerrRestant, j = countSend ; i < nbTerritoriesTotal ; i++, j++) territories_recv[j] = territories[i];
      }
      nbTerritories = nbTerrRestant+countSend;
    }

  // on indique où est le territoire maintenant
  for(int i = 0 ; i < nbTerritories ; i++) territories_recv[i].numProcessor = pid;


  /************************************
   ********* Envoie des liens. ********
   ************************************/
  MPI_Bcast(&nbLinks, 1, MPI_INT, 0, MPI_COMM_WORLD);
  if( pid != 0) links = new Link_territories[nbLinks];
  MPI_Bcast(links, nbLinks, MPI_LINK, 0, MPI_COMM_WORLD);


  /**********************************
   ****** Début de la partie. *******
   **********************************/
  
  // Envoie le nombre de joueurs à tous les processus.
  MPI_Bcast(&nbPlayers, 1, MPI_INT, 0, MPI_COMM_WORLD);
  
  while( !finJeu ) 
  {

    /**********************************
     ***** Ecriture de la map.   ******
     **********************************/
    if( pid == 0 ) printMap(territories, nbTerritoriesTotal, links_cpp, players, nbPlayers, etape);
    etape++;


    /**********************************
     ********** Statistique   **********
     **********************************/
    // Chaque processeur calcule les info qu'il a sur les joueurs.
    int nbUnites, nbUnitesTotal = 0;
    // Pour les territoires connexes, les processus on besoin de la carte complette pour leur calculs.
    // On envoit le nombre total de territoies.
    MPI_Bcast(&nbTerritoriesTotal, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if( pid != 0 ) territories = new Territory_c[nbTerritoriesTotal];
    MPI_Bcast(territories, nbTerritoriesTotal, MPI_TERRITORY, 0, MPI_COMM_WORLD);

    for(int i = 0 ; i < nbPlayers ; i++)
    {   
      nbUnites = 0;
      infoPlayer(territories_recv, nbTerritories, i+1, nbUnites);
      // Les processus envoit leur résultat au processeur 0, qui fait la somme.
      MPI_Reduce(&nbUnites, &nbUnitesTotal, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);  
      if( pid == 0 ) 
      {
        players[i].nbUnites = nbUnitesTotal;
        players[i].connexes.clear();
      }

      //connexe(i+1, territories, nbTerritoriesTotal, territories_recv, nbTerritories, links, nbLinks, players[i]);
    }

    if( pid != 0)
    {
      delete [] territories;
      territories = NULL;
    }
    
    
    

    /* 
        ******** Ordre de déplacement des joueurs. ********
        *
        * Le processus 0 demande les ordres de déplacements ;
        * Puis il determine le type de déplacement et l'envoit aux autres processus.
        * Cela permet de faciliter le traitement des déplacements.
        * 
        * Les déplacements peuvent être de trois types :
        *   - Entres deux territoires appartenant au même joueur.
        *   - Entre un territoire appartenant à un joueur et un terrain vide.
        *   - Entres un territoire appartenant au joueur et un territoire adverse.
        */

    if( pid == 0 ) 
    {
      mouv.clear();
      for(int i = 0 ; i < nbPlayers ; i++)
      {
        statistique(players, nbPlayers);
        char c = ' ';
        do {
          Mouvement m;

          // On demande à l'utilisateur ses mouvements.
          #ifdef SCENARIO
            mouvementTroupesScenario(territories, nbTerritoriesTotal, links, nbLinksTotal, players[i], m);
          #else
            mouvementTroupes(territories, nbTerritoriesTotal, links, nbLinksTotal, players[i], m);
          #endif

          // On détermine le type des mouvements.
          if( deplacement(players,nbPlayers,m.player,m.depart, m.arrivee) ) m.type = 1;
          else if( acquisition(players,nbPlayers,m.arrivee) ) m.type = 2;
          else m.type = 3;

          mouv.push_back(m);

          #ifdef SCENARIO
            c = 'n';
          #else
            cout << "Souhaitez-vous faire d'autre mouvement ? ('n' pour arrêter.)" << endl;
            cin >> c;
          #endif
        }while(c != 'n');
      }

      nbMvs = mouv.size();
      mvs = &mouv.front();
    }

    // Envoie des ordres à tous les processus.
    MPI_Bcast(&nbMvs, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if(pid != 0) mvs = new Mouvement[nbMvs];
    MPI_Bcast(mvs, nbMvs, MPI_MOUVEMENT, 0, MPI_COMM_WORLD);


    // Gestion des ordres par les processus concernés.
    for(int i = 0 ; i < nbMvs ; i++) 
    {
      //----- Cas où les deux territoires appartiennent au même joueur.
      if( mvs[i].type == 1 ) 
      {
        int index = 0;
        if( pid == quelProc(territories_recv, nbTerritories, mvs[i].depart, index) ) 
          territories_recv[index].units -= mvs[i].nbUnites;
	    
	       if( pid == quelProc(territories_recv, nbTerritories, mvs[i].arrivee, index) ) 
	         territories_recv[index].units += mvs[i].nbUnites;
	    }
		
      //----- Cas ou le territoire d'arrivee appartient à personne.
	    else if( mvs[i].type == 2 )
	    {
	      int index=0;

        // On met à jour le territoire d'arrivee.
	      if( pid == quelProc(territories_recv, nbTerritories, mvs[i].arrivee, index) )
	      { 
		      territories_recv[index].player = mvs[i].player;
		      territories_recv[index].units = mvs[i].nbUnites;
	      }

        // On met à jour le territoire de départ.
	      if( pid == quelProc(territories_recv, nbTerritories, mvs[i].depart, index) )
	        territories_recv[index].units -= mvs[i].nbUnites;

        // On met à jour le joueur.
        if( pid == 0 ) ajouterTerritoire(players, nbPlayers, mvs[i].player, mvs[i].arrivee);
	    }

      //----- Cas où il y a un combat.
      else if( mvs[i].type == 3)
      {
        int index=0;
        // On met à jour le territoire de départ.
        if( pid == quelProc(territories_recv, nbTerritories, mvs[i].depart, index) )
          territories_recv[index].units -= mvs[i].nbUnites;
      }
    }


    // Les processus renvoient leurs territoires aux processus 0.
    if( pid != 0 ) MPI_Send(territories_recv, nbTerritories, MPI_TERRITORY, 0, 1, MPI_COMM_WORLD);
    else 
    {
      territories = new Territory_c[nbTerritoriesTotal];
      for(int i = 0 ; i < nbTerritories ; i++) territories[i] = territories_recv[i];
      for(int i = 1, j = nbTerritories; i < nbProcs ; i++, j+=countSend) MPI_Recv(territories+j, countSend, MPI_TERRITORY, i, 
                                                                                    1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      /**********************************
       ****** Gestion des combats. *******
       **********************************/
      for(int i = 0 ; i < nbMvs ; i++)
      {
        if( mvs[i].type == 3 ) // Combat !
        {
          int terriAttaque = 0;
          int nbTroupeJoueurA = mvs[i].nbUnites, desA = 0;
          int nbTroupeJoueurB = nbUniteInTerri(territories, nbTerritoriesTotal, mvs[i].arrivee, terriAttaque);
          int desB = 0;

          while( nbTroupeJoueurA > 0 && nbTroupeJoueurB > 0 ) {
              #ifdef SCENARIO
                nbTroupeJoueurA--;
                nbTroupeJoueurB--;
              #else
                desA = rand()% 6;
                desB = rand()% 6;
                if( desA < desB ) nbTroupeJoueurA--;
                else if( desB < desA ) nbTroupeJoueurB--;
              #endif
            }
            
            // Le joueur qui attaque a perdu.
            if( nbTroupeJoueurA == 0) 
            {
              territories[terriAttaque].units = nbTroupeJoueurB;
            }
            // Le joueur qui attaque a gagné.
            else
            {
              int autreJoueur = territories[terriAttaque].player;
              territories[terriAttaque].player = mvs[i].player;
              territories[terriAttaque].units = nbTroupeJoueurA;
              ajouterTerritoire(players, nbPlayers, mvs[i].player, mvs[i].arrivee);
              enleverTerritoire(players, nbPlayers, autreJoueur, mvs[i].arrivee); 
            }
          }
      }

      // Le processus 0 test la fin du jeu et l'envoit aux autres processus.
      if( gameEnd(players, nbPlayers) != -1 ) 
      {
        finJeu = 1;
        cout << "Le joueur " << gameEnd(players, nbPlayers) << " a gagné ! " << endl;
      }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&finJeu, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if( !finJeu ) {

      /**********************************
      ****** Renfort de troupes   *******
      **********************************/
      if( pid == 0) {
        for(int i = 0 ; i < nbTerritoriesTotal ; i++) 
        {
          //cout << "Avant renfort (" << territories[i].id << ") =" << territories[i].units << endl;
          int renfort = territories[i].units / 2;
          territories[i].units += renfort;
          //cout << "Après renfort (" << territories[i].id << ") =" << territories[i].units << endl;
        }
      }

      // On redistribue les territoires.
      MPI_Bcast(&nbTerritoriesTotal, 1, MPI_INT, 0, MPI_COMM_WORLD);
      countSend = nbTerritoriesTotal % nbProcs;
      territories_recv = new Territory_c [countSend];
  
      if( pid != 0 )
        nbTerritories = countSend;

      MPI_Scatter(territories, countSend, MPI_TERRITORY, 
                  territories_recv, countSend, MPI_TERRITORY, 0, MPI_COMM_WORLD); 
  
      if(pid == 0)
      {
        int nbTerrRestant = nbTerritoriesTotal - (countSend*(nbProcs-1))-countSend;

        if( nbTerrRestant > 0 )
        {
          Territory_c * terAux = (Territory_c*) realloc(territories_recv,sizeof(Territory_c)*(countSend+nbTerrRestant));
          territories_recv = terAux;
          for(int i = 0 ; i < countSend ; i++ ) territories_recv[i] = territories[i]; 
          for(int i = nbTerritoriesTotal - nbTerrRestant, j = countSend ; i < nbTerritoriesTotal ; i++, j++) territories_recv[j] = territories[i];
        }
        nbTerritories = nbTerrRestant+countSend;
      }

      // on indique où est le territoire maintenant
      for(int i = 0 ; i < nbTerritories ; i++) territories_recv[i].numProcessor = pid;
    }
  }
  
  MPI_Finalize();
  return 0;
}
