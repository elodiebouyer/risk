#ifndef _TERRITOIRE_HPP
#define _TERRITOIRE_HPP

#include <stdlib.h>
#include <iostream>
#include <vector>
#include "mpi.h"

using namespace std;

struct Link_territories
{ 
  int idA;
  int idB;
};

struct Territory_c
{
  int id;
  int player;
  int units;
  int numProcessor;
};

Territory_c & createTerritory(int id);

void printTerritory(Territory_c c);

void buildTerritoryType(MPI_Datatype * MPI_TERRITORY_ptr);

void buildLinkType(MPI_Datatype * MPI_LINKS_ptr);

/**
 * Permet de connaitre le numéro du processeur qui possède id.
 * @param ters Liste des territoires.
 * @param nb Nombre de territoire dans la liste.
 * @param id Identifiant du territoire qu'on recherche.
 */
int quelProc(Territory_c * ters, int nb, int id, int & index);

/**
 * Permet de connaitre le nombre d'unité se trouvant sur un terrain donné.
 * @param ters Liste des territoires.
 * @param nb Nombre de territoire dans la liste.
 * @param num Numéro du territoire.
 * @param ter Position du territoire num.
 */
int nbUniteInTerri(Territory_c * ters, int nb, int num, int & ter);

/**
 * Permet de savoir s'il existe un passage entre deux terrritoires.
 * @param links Liste de tous les liens entres les territoires.
 * @param nbLinks Nombre de liens dans la liste.
 * @param depart Territoire de départ.
 * @param arrivee Territoire d'arrivée.
 *
 * @return true s'il existe un lien, false sinon.
 */
bool passage(Link_territories *links, int nbLinks, int depart,int arrivee);

/**
* Permet de savoir si il y a assez d'unité sur un terrain pour être déplacé.
* @param ters Liste des territoires.
* @param nbTers Nombre de territoire.
* @param id Identifiant du territoire dont on veut enlever des unités.
* @param nbUnite Nombre d'unité que l'on souhaite déplacer.
*/
bool uniteSuffisante(Territory_c * ters, int nbTers,int id, int nbUnite);

/**
* Determine si un territoire appartient à un joueur.
* @param player Identifiant du joueur.
* @param ter Identifiant du territoire.
* @param territories Liste des territoires.
* @param nbTerri Nombre de territoire dans la liste.
*
* @return Vrai si le territoire appartient au joueur, faux sinon.
*/
bool estAuJoueur(int player, int ter, Territory_c * territories, int nbTerri, int & proprietaire);

#endif
