#include "Joueur.hpp"
#include "Utils.hpp"

using namespace std;


Color getColorPlayer(Player * players, int nbPlayer, int id)
{
  for(int i = 0 ; i < nbPlayer ; i++)
    if( players[i].id == id) return players[i].color;
}

Player * askPlayers(int nb, Player * p)
{
  #ifdef SCENARIO
    p[0] = createPlayer("player1", (Color)1, 1);
    p[1] = createPlayer("player2", (Color)2, 2);
    return p;
  #else
    string nom;
    Color col;
    int color;

    for(int i = 0 ; i < nb ; i++){
      cout << "Nom du joueur " << i+1 << endl;
      cin >> nom;
      
      cout << "Couleur du joueur " << i+1 << endl;
      printColors();
      cin >> color;
      col = (Color) (color%7);

      p[i] = createPlayer(nom, col, i+1);
    }
    return p;
  #endif
}


Player createPlayer(string name, Color c, int id)
{
  Player p;
  p.id = id;
  p.name = name;
  p.color = c;
  p.territories.clear();
  p.nbTerritories = 0;
  p.nbUnites = 0;
  return p;
}

void printPlayerName(Player p)
{
  cout << p.name << endl;
}


void ajouterTerritoire(Player * players, int nb, int idPlayer, int idTerri)
{
  for(int i = 0 ; i < nb ; i++)
  {
    if( players[i].id == idPlayer )
    {
      players[i].territories.push_back(idTerri);
      players[i].nbTerritories++;
    }
  }
}

void enleverTerritoire(Player * players, int nb, int idPlayer, int idTerri)
{
  for(int i = 0 ; i < nb ; i++)
  {
    if( players[i].id == idPlayer )
    {
      for(int j = 0 ; j < players[i].nbTerritories ; j++)
      {
        if( players[i].territories[j] == idTerri ) 
        {
          players[i].territories.erase(players[i].territories.begin()+j);
          if( players[i].nbTerritories > 0 ) players[i].nbTerritories--;
          else players[i].nbTerritories = 0;
        }
      }
    }
  }
}


void printPlayerTerritories(Player p)
{
  if( p.nbTerritories == 0 )
    {
      cout << "Le joueur " << p.name << " n'a aucun territoire." << endl;
      return;
    }

  cout << "Le joueur " << p.name << " a " << p.nbTerritories << " territoires : ";
  for(int i = 0 ; i < p.nbTerritories ; i ++)
    cout << "Territoire n°"<< p.territories[i] << "  ";
  cout << endl;
}

bool lose(Player p)
{
  if(p.nbTerritories == 0) return true;
  else if( p.nbUnites == 1 ) return true;
  return false;
}


int gameEnd(Player * p, int nbPlayer)
{
  int nbLose = 0;
  int winner = -1;
  for(int i = 0 ; i < nbPlayer ; i++)
    {
      if( lose(p[i]) ) nbLose++;
      else winner = i;
    }
  if( nbLose == nbPlayer-1 ) return winner;
  return -1;
}
  
bool aTerritoire(Player p, int t)
{
  for(int i = 0; i < p.nbTerritories ; i++)
    if( p.territories[i] == t) return true;
  return false;
}


bool deplacement(Player* ps, int nb, int p, int t1, int t2)
{
  for(int i = 0 ; i < nb ; i++)
    if( ps[i].id == p) {
      if( aTerritoire(ps[i], t1) && aTerritoire(ps[i], t2)) return true;
      return false;
    }
}

bool acquisition(Player* ps, int nb, int terri)
{
  for(int i = 0 ; i < nb ; i++ )
    for(int j = 0 ; j < ps[i].nbTerritories ; j++)
      if( ps[i].territories[j] == terri) return false;
  return true;
}

bool doublon(Player p, int * voisin, int nbVoisin)
{
  for(int i = 0 ; i < p.connexes.size() ; i++)
  {
    for(int j = 0 ; j < p.connexes[i].size() ; j++)
    {
      if( p.connexes[i][j] == voisin[0] ) return true;
    }
  }
  return false;
}

void addConnexes(Player & player, int * voisin, int nbVoisin)
{
  std::vector<int> v;

  if( nbVoisin == 0 ) return;
  if( doublon(player, voisin, nbVoisin) ) return;

  for(int i = 0 ; i < nbVoisin ; i++)
  {
    if( voisin[i] == -1 )
    {
      if( !v.empty() ) player.connexes.push_back(v);
      v.clear();
    }
    else v.push_back(voisin[i]);
  }
  if( !v.empty() ) player.connexes.push_back(v);
}

void afficherTerritoireConnexe(Player p)
{
  if( p.connexes.size() == 0 ) 
  {
    cout << "Auccun." << endl;
    return ;
  }
  for(int i = 0 ; i < p.connexes.size() ; i++)
  {
    cout << "\t - Vous avez un groupe de " << p.connexes[i].size() << " territoires connexes : ";
    for(int j = 0 ; j < p.connexes[i].size() ; j++) cout << p.connexes[i][j] << " ";
    cout << endl;
  }
}

void statistique(Player *  players, int nbPlayers)
{
  system("clear");
  for(int i  = 0 ; i < nbPlayers ; i++ )
  {
    cout << endl;
    cout << " ------------------ " << endl;
    cout << endl;
    cout << "Statistique du joueur : " << players[i].name << endl;
    cout << endl;
    cout << "Couleur du joueur : " << colorToString(players[i].color) << endl;
    cout << "Nombre d'unités total : " << players[i].nbUnites << endl;
    cout << "Nombre de territoire : " << players[i].nbTerritories << endl;
    /*cout << "Nombre de territoire connexes : " << endl;
    afficherTerritoireConnexe(players[i]);*/
  }
}