#ifndef _UTILS_HPP
#define _UTILS_HPP

#include "Joueur.hpp"

#define FIRST_TERRITORIES 5
#define FIRST_UNITS 4


struct Mouvement 
{
  int player;
  int depart;
  int arrivee;
  int nbUnites;
  int type;
};

/**
* Affiche la liste des couleurs disponible à l'écran.
*/
void printColors();


/**
* Convertie un entier en une chaine de caractère.
*
* @param entier Entier que l'on veut convertir.
*
* @return La chaine de caractère correspondant.
*/
string intToString(int entier);


/**
* Convertie un entier en une chaine de caractère.
*
* @param c Couleur que l'on veut convertir.
* 
* @return La chaine de caractère correspondant.
*/
string colorToString(Color c);

/**
 * Demande à l'utilisateur combien de joueur il veut pour la partie.
 * @return Le nombre de joueur.
 */
int howManyPlayer();

/**
 * Charge un fichier .dot et construit la map.
 */
vector<Territory_c> loadMap(vector<Link_territories>* links);

/**
* Construit un fichier .dot à partir des données sur les territoires et les joueurs.
* @param ters Liste de tous les territoires.
* @param nbTerritories Nombre de territoire.
* @param links Liste des liens entres les territoires.
* @param players Liste des joueurs.
* @param nbPlayers Nombre de joueur.
* @param etape Numéro de l'étape.
*/
void printMap(Territory_c *ters, int nbTerritories, vector<Link_territories> links, Player * players, int nbPlayers, int etape);

void afficherTerritoires(int nb, Territory_c * territories);
void afficherLink(int nb, Link_territories * links);
void afficherPlayers(int nb, Player *p);

/**
 * Permet à un joueur de donner ses ordres de déplacement.
 * @param p Le joueur qui doit donner ses ordres.
 */
 #ifndef SCENARIO
void mouvementTroupes(Territory_c *terri, int nbTerri, Link_territories *links, int nbLinks, Player p, Mouvement & m);
#else
void mouvementTroupesScenario(Territory_c * terri, int nbTerri, Link_territories * links,int nbLinks,Player p, Mouvement & m);
#endif
/*
 * Permet de créer le nouveau type dérivé.
 * @param m Type que l'on veut créer.
 */
void buildMouvement(MPI_Datatype * m);

void afficherOrdre(Mouvement* mvs,int nb, int pid);

void infoPlayer(Territory_c * territories, int nbTerritories, int id, int & nbUnites);

void connexe(int player, Territory_c *territories, int nbTerritoriesTotal, 
                         	Territory_c *territories_recv, int nbTerritories, 
                         	Link_territories *links, int nbLinks,
                         	Player & p);

void afficherGrappe(int * voisin, int nbVoisin);

#endif
