#include "Territoire.hpp"

Territory_c & createTerritory(int id){
  Territory_c * c = (Territory_c*)malloc(sizeof(Territory_c));
  c->id = id; 
  c->player = -1;
  c->units = 0;
  c->numProcessor = -1;
  return *c;
}

void printTerritory(Territory_c c){
  cout << "Territoire " << c.id << " appartient à " << c.player << ", contient " << c.units << " unités et est sur le proc " << c.numProcessor << endl;
}

void buildTerritoryType(MPI_Datatype * MPI_TERRITORY_ptr)
{
  int block_lengths[4]={1,1,1,1};
  MPI_Aint deplacement[4];
     
  MPI_Datatype typelists[4] = {MPI_INT,MPI_INT,MPI_INT,MPI_INT};
  MPI_Aint start_address;
  MPI_Aint address;

  //Calculate Displacements
  deplacement[0] = offsetof(Territory_c, id);
  deplacement[1] = offsetof(Territory_c, player);
  deplacement[2] = offsetof(Territory_c, units);
  deplacement[3] = offsetof(Territory_c, numProcessor);

  //Create and Commit new mpi type
  MPI_Type_create_struct(4, block_lengths,deplacement,typelists,MPI_TERRITORY_ptr);
    
}

void buildLinkType(MPI_Datatype * MPI_LINKS_ptr)
{
  int block_lengths[2]={1,1};
  MPI_Aint deplacement[2];
     
  MPI_Datatype typelists[2] = {MPI_INT,MPI_INT};
  MPI_Aint start_address;
  MPI_Aint address;
 
  //Calculate Displacements
  deplacement[0] = offsetof(Link_territories, idA);
  deplacement[1] = offsetof(Link_territories, idB);

  //Create and Commit new mpi type
  MPI_Type_create_struct(2,block_lengths,deplacement,typelists,MPI_LINKS_ptr);
    
}

int quelProc(Territory_c * ters, int nb, int id, int & index)
{
  for(int i = 0; i < nb ; i++ )
    if( ters[i].id == id ) {
      index = i;
      return ters[i].numProcessor;
    }
  return -1;
}

int nbUniteInTerri(Territory_c * ters, int nb, int num, int & ter)
{
  for(int i = 0 ; i < nb ; i++) 
    if( num == ters[i].id ) {
      ter = i;
      return ters[i].units;
  }
}


bool passage(Link_territories * links, int nbLinks, int depart,int arrivee)
{
  for(int i = 0 ; i < nbLinks ; i++)
    if(links[i].idA == depart && links[i].idB == arrivee)
      return true;
    else if(links[i].idA == arrivee && links[i].idB == depart)
      return true;
  return false;
}


bool uniteSuffisante(Territory_c * ters, int nbTers,int id, int nbUnite)
{
  for(int i = 0 ; i < nbTers ; i++)
  {
    if( ters[i].id == id )
    {
      if( ters[i].units > nbUnite) return true;
      else return false;
    }
  }
  return false;
}

bool estAuJoueur(int player, int ter, Territory_c * territories, int nbTerri, int & proprietaire)
{
  if( territories == NULL ) return false;
  
  for(int i = 0 ; i < nbTerri ; i++)
  {
    if( territories[i].id == ter )
    {
      if( territories[i].player == player) return true;
      else proprietaire = territories[i].player;
      return false;
    }
  }
  return false;
}