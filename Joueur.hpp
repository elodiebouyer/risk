#ifndef _JOUEUR_HPP
#define _JOUEUR_HPP

#include "Territoire.hpp"

#include <vector>
#include <string>
#include <iostream>

using namespace std;

enum Color 
  {
    Blue,
    Red,
    Green,
    Pink,
    Brown,
    Orange,
    Yellow
  };

struct Player
{
  int id;
  string name;
  Color color;
  vector<int> territories;
  int nbTerritories;
  int nbUnites;
  vector<vector<int> > connexes;
};

/**
* Permet d'obtenir la couleur d'un joueur celon son id.
* @param players Liste de joueur.
* @param nbPlayer Nombre de joueur.
* @param id Identifiant du joueur dont on veut la couleur.
* @return La couleur du joueur.
*/
Color getColorPlayer(Player * players, int nbPlayer, int id);

/**
 * Demande le nom et la couleur de l'utilisateur.
 */
Player * askPlayers(int nb, Player * p);

/**
 * Crée un joueur.
 */
Player createPlayer(string name, Color c, int id);

/**
 * Affiche le nom d'un joueur.
 */
void printPlayerName(Player p);

/**
 * Ajoute un territoire à un joueur.
 * @param players Liste des joueurs.
 * @param nb Nombre de joueur.
 * @param idPlayer Identifiant du joueur dont on veut ajouter un territoire.
 * @param idTerri Identifiant du territoire à ajouter.
 */
void ajouterTerritoire(Player * players, int nb, int idPlayer, int idTerri);

/**
 * Enlève un territoire à un joueur.
 * @param players Liste des joueurs.
 * @param nb Nombre de joueur.
 * @param idPlayer Identifiant du joueur dont on veut enlever un territoire.
 * @param idTerri Identifiant du territoire à enlever.
 */
 void enleverTerritoire(Player * players, int nb, int idPlayer, int idTerri);

/**
 * Affiche la liste des territoires d'un joueur.
 * @param p Le joueur.
 */
void printPlayerTerritories(Player p);

/**
 * Permet de savoir si le joueur p à perdue.
 * @param p Le joueur.
 * @return true si le joueur à perdue, faux sinon.
 */
bool lose(Player p);

/**
 * Permet de savoir si la partie est finie.
 * @param p Liste des joueurs.
 * @param nbPlayer Nombre de joueurs.
 * @return -1 si la partie n'est pas encore finie, le numéro du gagnant sinon.
 */
int gameEnd(Player * p, int nbPlayer);

/**
 * Permet de savoir si un joueur a le territoire t ou non.
 *
 * @param p Le joueur.
 * @param t Le territoire.
 *
 * @return true si le joueur possède le territoire, false sinon.
 */
bool aTerritoire(Player p, int t);

bool deplacement(Player* ps, int nb, int p, int t1, int t2);

bool acquisition(Player* ps, int nb, int terri);

/**
* Ajoute les grapes de territoires connexe que possède un joueur.
*
* @param player Le joueur dont on veut ajoueur une grape.
* @param voisin La liste de territoires que l'on veut ajouter.
* @param nbVoisin Le nombre de territoires dans la liste.
*/
void addConnexes(Player & player, int * voisin, int nbVoisin);

void afficherTerritoireConnexe(Player p);

void statistique(Player *player, int nbPlayers);

#endif 
